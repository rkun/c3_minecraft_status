import React from 'react';
import './App.css';
import MCS from './components/MCS.jsx';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MCS server_fqdn="minecraft.compositecomputer.club"></MCS>
      </header>
    </div>
  );
}

export default App;
