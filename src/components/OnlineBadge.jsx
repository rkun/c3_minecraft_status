import React from 'react';
import style from '../css/OnlineBadge.module.css';


export default function OnlineBadge(props) {

  const getText = () => (props.active ? "Online": "Offline")
  const getClassName = () => (props.active ? style.active: style.inactive)

  return (
    <span className={style.badge+" "+getClassName()}>
      {getText()}
    </span>
  )
}
