import React, {useState, useEffect} from 'react';
import style from '../css/Player.module.css';


export default function Player(props) {
  const [imgUrl, setImgUrl] = useState("");
  useEffect(() => {
    setImgUrl(`https://minotar.net/bust/${props.name}/100.png`);
    console.log(props);
  }, [props]);

  /*
  const componentStyle = {
    "display": "inline-block"
  }
  */

  return (
    <div className={style.card}>
      <img className={style.image} src={imgUrl} alt=""></img>
      <div className={style.name}>{props.name}</div>
    </div>
  )
}
