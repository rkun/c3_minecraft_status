import React, {useState, useEffect} from 'react';
import Player from './Player.jsx';
import OnlineBadge from './OnlineBadge.jsx';
import style from '../css/MCS.module.css';


export default function MCS(props) {
  const [serverState, setServerState] = useState({});
  const [playerList, setPlayerList] = useState([]);

  useEffect(() => {
    const fetchApi = ()=>{
      fetch(`https://api.mcsrvstat.us/2/${props.server_fqdn}`)
        .then(res => {
          return res.json()
        })
        .then(obj => {
          setServerState(obj);
          if(obj.players !== undefined && obj.players.online > 0) setPlayerList(obj.players.list);
          else setPlayerList([]);
        })
    };

  
    console.log("useEffect");
    fetchApi();
    const apiSubscribe = setInterval(fetchApi, 60000);
    return () => {
      clearInterval(apiSubscribe);
    }
  }, [props.server_fqdn])

  
  const playerListElem = () => {
    return playerList.map(player => (
        <Player name={player}></Player>
        //player
      )
    )
  };

  const updatePageTitle = () => { document.title = `Status: ${serverState.hostname}` }

  return (
    <div>
      {updatePageTitle()}
      <h1>{serverState.hostname}</h1>
      <div className={style.property}>State: {<OnlineBadge active={serverState.online}></OnlineBadge>}</div>
      <div className={style.property}>Ver: {serverState.version}</div>
      <div className={style.playerList}>
        {playerListElem()}
      </div>
    </div>
  )
}
